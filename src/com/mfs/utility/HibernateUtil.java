package com.mfs.utility;
import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.mfs.pojo.BankAccount;

/**
 * utility class
 * @author Shrikant
 *
 */
public class HibernateUtil {
    private static SessionFactory sessionFactory;
    
    /**
	 * method to create session factory
	 * @return session factory
	 */
    public static void initializeSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration();

                // Hibernate settings equivalent to hibernate.cfg.xml's properties
                Properties settings = new Properties();
                settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
                settings.put(Environment.URL, "jdbc:mysql://localhost:3308/bank");
                settings.put(Environment.USER, "root");
                settings.put(Environment.PASS, "root");
                settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");

                settings.put(Environment.SHOW_SQL, "true");

                settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

               // settings.put(Environment.HBM2DDL_AUTO, "create-drop");

                configuration.setProperties(settings);
                configuration.setProperty("cache.use_second_level_cache", "true")
                .setProperty("cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
                
                //configuration.addAnnotatedClass(Customer.class);
                //configuration.addAnnotatedClass(Accounts.class);

                configuration.addAnnotatedClass(BankAccount.class);
                
                

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

	public static SessionFactory getSessionFactory(){
		return sessionFactory;
	}
}