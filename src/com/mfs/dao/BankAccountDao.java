package com.mfs.dao;

public interface BankAccountDao {

	/**
	 * method to encrypt the password
	 */
	public void encyptPwd(String username,String pwd);
	/**
	 * method to decrypt the password provided by the id
	 * @param id user id
	 */
	public void decryptPwd(int id);
}
