package com.mfs.dao;

import javax.transaction.Transactional;

import org.hibernate.Session;

import com.mfs.pojo.BankAccount;
import com.mfs.utility.HibernateUtil;

/**
 * bankaccount implementaton / this class is used to connect with database
 * 
 * @author shrikant
 *
 */
public class BankAccountDaoImpl implements BankAccountDao {

	private Session session = null;

	public BankAccountDaoImpl() {
		session = HibernateUtil.getSessionFactory().openSession();
	}

	@Transactional
	public void encyptPwd(String username,String pwd) {
		session.beginTransaction();
		BankAccount account = new BankAccount();
		account.setUsername(username);
		account.setPassword(pwd);
		session.save(account);
		session.getTransaction().commit();
		session.close();
	}

	@Transactional
	public void decryptPwd(int id) {
		BankAccount account =findById(id);
		System.out.println(account.getUserid());
		System.out.println(account.getUsername());
		System.out.println(account.getPassword());
		session.close();

	}
	/**
	 * method to find accounts by account number
	 * @param accno account number
	 * @return
	 */
	public BankAccount findById(Integer userid){
		BankAccount accounts = null;
		session.beginTransaction();
		accounts = (BankAccount)session.load(BankAccount.class, userid);
		return accounts;
	}
	
}