package com.mfs.service;

import javax.jws.WebService;

import com.mfs.dao.BankAccountDaoImpl;
import com.mfs.utility.HibernateUtil;

/**
 * class for service implementation
 * @author Shrikant
 *
 */
@WebService
public class BankAccountServiceImpl implements BankAccountService {

	@Override
	public void encyptPwd(String username,String pwd) {
		new BankAccountDaoImpl().encyptPwd(username,pwd);

	}

	@Override
	public void decryptPwd(int id) {
		new BankAccountDaoImpl().decryptPwd(id);
	}

	public void connectDB(){
		HibernateUtil.initializeSessionFactory();
	}
}
