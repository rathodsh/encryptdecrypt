package com.mfs.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnTransformer;

@Entity
@Table(name = "user")
public class BankAccount {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
	private int userid;
	@Column
	private String username;
	@ColumnTransformer( read="AES_decrypt(password ,'00')" ,write="AES_encrypt(? , '00')")
	private String password;
	public BankAccount() {
	}
	
	public BankAccount(int userid, String username, String password) {
		super();
		this.userid = userid;
		this.username = username;
		this.password = password;
	}
	

	public int getUserid() {
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "BankAccount [userid=" + userid + ", username=" + username + ", password=" + password + "]";
	}
	
	
	
}
